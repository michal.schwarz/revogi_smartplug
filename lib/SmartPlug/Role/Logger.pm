package SmartPlug::Role::Logger;

use utf8;
use Moose::Role;

use Log::Any ();

has logger => (
    is      => 'ro',
    default => sub { Log::Any->get_logger },
);

1;