package SmartPlug::Integration;

use utf8;
use Moose;
use MooseX::StrictConstructor;

use SmartPlug::Integration::UDP;
use JSON;
use DDP;

with qw/SmartPlug::Role::Logger/;

has remote_host => (is => 'rw', isa => 'Str', required => 1);

has json => (is => 'ro', isa => 'JSON',                        lazy_build => 1);
has sn   => (is => 'rw', isa => 'Str',                         lazy_build => 1);
has udp  => (is => 'rw', isa => 'SmartPlug::Integration::UDP', lazy_build => 1);

has cmd_ping     => (is => 'ro', isa => 'Str', default => '00sw=all,,,;');
has cmd_turn_on  => (is => 'ro', isa => 'Str', default => 'V3{"sn":"--SERIAL--","cmd":20,"port":0,"state":1}');    # port=0 means "all ports"
has cmd_turn_off => (is => 'ro', isa => 'Str', default => 'V3{"sn":"--SERIAL--","cmd":20,"port":0,"state":0}');    # port=0 means "all ports"
has cmd_status   => (is => 'ro', isa => 'Str', default => 'V3{"sn":"--SERIAL--","cmd":90}');

sub _build_json {
    return JSON->new->utf8(1);
}

sub _build_udp {
    my $self = shift;
    return SmartPlug::Integration::UDP->new(remote_host => $self->remote_host);
}

sub _build_sn {
    my $self = shift;

    my $raw_response = $self->udp->send_and_receive($self->cmd_ping);

    # die "No response from " . $self->remote_host unless $raw_response;
    if (!$raw_response) {
        $self->logger->warning("No response from " . $self->remote_host);
        return '';
    }

    my $data = $self->json->decode($raw_response);
    my $sn   = $data->{data}->{sn};

    return $sn;
}

sub individualized_command {
    my $self = shift;
    my $cmd  = shift;

    my $sn = $self->sn;
    $cmd =~ s/--SERIAL--/$sn/g;
    return $cmd;
}

sub v3_command_json_response {
    my $self       = shift;
    my $v3_command = shift;

    my $cmd          = $self->individualized_command($v3_command);
    my $raw_response = $self->udp->send_and_receive($cmd);           # V3{"response":20,"code":200} etc.

    $raw_response =~ s/^V3//;
    if (($raw_response // '') eq '') {
        $self->logger->warning("No response from " . $self->remote_host);
        return { code => 500 };
    }

    my $data = $self->json->decode($raw_response);
    return $data;
}

sub turn_device_on {
    my $self = shift;
    my $data = $self->v3_command_json_response($self->cmd_turn_on);
    return $data->{code} == 200;
}

sub turn_device_off {
    my $self = shift;
    my $data = $self->v3_command_json_response($self->cmd_turn_off);
    return $data->{code} == 200;
}

sub device_status {
    my $self = shift;
    my $data = $self->v3_command_json_response($self->cmd_status);

    return {
        amps   => undef,
        watts  => undef,
        switch => undef
        }
        unless $data->{code} == 200;

    # for a single-port device only:
    return {
        amps   => $data->{data}->{amp}->[0] / 1000,
        watts  => $data->{data}->{watt}->[0] / 1000,
        switch => $data->{data}->{switch}->[0],
    };
}

__PACKAGE__->meta->make_immutable;

1;
