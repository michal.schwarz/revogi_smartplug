package SmartPlug::Integration::UDP;

use utf8;
use Moose;
use MooseX::StrictConstructor;

use IO::Socket;
use IO::Socket::Timeout;

with qw/SmartPlug::Role::Logger/;

has _remote_port   => (is => 'ro', isa => 'Str', default => 8888);
has _read_timeout  => (is => 'ro', isa => 'Num', default => 0.5);
has _write_timeout => (is => 'ro', isa => 'Num', default => 0.5);

has remote_host => (is => 'rw', isa => 'Str', required => 1);

sub send_and_receive {
    my $self    = shift;
    my $command = shift;

    my $sock = IO::Socket::INET->new(
        Proto    => 'udp',
        PeerPort => $self->_remote_port,
        PeerAddr => $self->remote_host,
    ) or die "Could not create socket: $!\n";

    IO::Socket::Timeout->enable_timeouts_on($sock);
    $sock->read_timeout($self->_read_timeout);
    $sock->write_timeout($self->_write_timeout);

    $self->logger->debug("sending data");
    $sock->send($command) or die "Send error: $!\n";

    my $received_data;

    my $status = $sock->recv($received_data, 1024);

    $self->logger->error("No data received") unless defined $status;

    return $received_data;
}

__PACKAGE__->meta->make_immutable;

1;