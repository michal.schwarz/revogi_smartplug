# About

This HTTP REST API allows the "E.ON Chytrá zásuvka" (alias Revogi Smart Plug) to be controlled via direct LAN access.


It is based on (now archived) https://github.com/andibraeu/revogismartstripcontrol/tree/master/src/main/kotlin/de/andi95/smarthome/revogismartstripcontrol

# Usage

```bash
$ HOST=ip.address.or.host.name

# Get device status
$ scripts/rest_server get -M GET /device/status/$HOST

# Turn the smart plug on
$ scripts/rest_server get -M POST /device/on/$HOST

# Turn if off
$ scripts/rest_server get -M POST /device/off/$HOST
```

or via REST API:

```bash
$ scripts/rest_server daemon  -l 'http://*:8081'

$ curl http://localhost:8081/device/status/$HOST
{"details":{"amps":0.015,"switch":1,"watts":0},"status":"ok"}

$ curl -X POST http://localhost:8081/device/on/$HOST
{"details":{},"status":"ok"}
```
